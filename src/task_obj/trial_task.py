import math

from src.finger_obj.finger_trial import Finger_Trial
import numpy as np

from src.utilities.const import MAX_FRAME
from src.utilities.face_recognition_utils import get_face_encoding_by_uid, compare_faces
from src.utilities.utils import logger


def choose_random_points(width, height, n=3):
    """
    It chooses n random points from a grid of size width x height

    :param width: the width of the image
    :param height: the height of the image
    :param n: number of points to choose, defaults to 3 (optional)
    :return: A list of tuples.
    """
    x_list = np.random.choice(width - 20, n)
    y_list = np.random.choice(height - 20, n)
    dots = [(int(x_list[i] + 20), int(y_list[i]) + 20) for i in range(n)]
    return dots


class TrailTask:

    def __init__(self, task_id, uid, width, height, radius=15):
        logger.info("new trail task")
        self.task_id = task_id
        self.trial_object = Finger_Trial()
        self.uid = uid
        self.win_status = False
        self.point_to_win = choose_random_points(width, height)
        self.task_progress = {i: {"status": False, "location": p, "radius": radius} for i, p in enumerate(self.point_to_win)}
        self.user_confirmed = False
        self.radius = radius
        self.count_frame = 0
        self.relative_count_frame = 0

    def full_operation(self, image):
        """
        It checks if the user has been confirmed, if not, it checks if the user is in the database, if so, it confirms the user and returns the JSON API.
        If the user is not confirmed, it checks if the user has been in the database for a certain amount of frames, if so, it returns the JSON API. If
        the user is confirmed, it checks if the user's hand is in the frame, if so, it checks if the user's hand is in the correct position, if so, it
        checks if the user has won, if so, it returns the JSON API

        :param image: the image that is being passed in
        :return: The return value is a JSON object that contains the following information:
            - The current frame count
            - The current score
            - The current status of the game (win, lose, or continue)
            - The current status of the hand (in or out)
            - The current status of the user (confirmed or not)
            - The current status of the trial
        """
        if not self.user_confirmed:
            self.user_confirmed = compare_faces(self.uid, image)
            if self.user_confirmed:
                return self.create_json_api()
            return None
        self.check_frame_count()
        new_point = self.trial_object.check_hand_status(image)
        if not new_point:
            return None
        self.check_single_frame(new_point)
        self.check_win_status()
        return self.create_json_api()

    def check_frame_count(self):
        """
        It checks the frame count and cleans the progress if the relative frame count is greater than the maximum frame
        """
        self.count_frame += 1
        self.relative_count_frame += 1
        if self.relative_count_frame > MAX_FRAME:
            logger.info("the client reach the max frames - cleaning progress")
            self.clean_progress()

    def clean_progress(self):
        """
        It takes the task_progress dictionary and sets the status of each task to False
        """
        for p in self.task_progress:
            self.task_progress[p]["status"] = False

    def check_single_frame(self, new_point):
        """
        If the new point is within the radius of a task point, mark that task point as complete

        :param new_point: the new point to check
        """
        if new_point:
            new_x, new_y = new_point
            for task_point_key in self.task_progress:
                task_point = self.task_progress[task_point_key]
                if task_point["status"]:
                    continue
                center_x, center_y = task_point["location"]
                dist = math.sqrt((center_x - new_x) ** 2 + (center_y - new_y) ** 2)
                if dist < self.radius:
                    task_point["status"] = True
                break

    def check_win_status(self):
        """
        If all the tasks are completed, then the win_status is set to True
        :return: The win_status is being returned.
        """
        for task_point_key in self.task_progress:
            if not self.task_progress[task_point_key]["status"]:
                return False
        self.win_status = True
        logger.info(f"The client complete task -{self.task_id}-!")
        return True

    def create_json_api(self):
        """
        It creates a dictionary with the task_id, status, uid, type, param_values, and user_confirmed
        :return: The return value is a dictionary with the following keys:
        """
        x = {
            "task_id": self.task_id,
            "status": self.win_status,
            "uid": self.uid,
            "type": "trial",
            "param_values": self.trial_object.fingers_points,
            "user_confirmed": self.user_confirmed

        }
        logger.info(x)
        return x


if __name__ == '__main__':
    logger.info("Debug Trial Task")
    x = TrailTask(100, 100)
