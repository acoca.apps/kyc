import random

from src.finger_obj.finger_symbol import Finger_Symbol

from src.utilities.const import SYMBOL, MAX_FRAME
from src.utilities.face_recognition_utils import get_face_encoding_by_uid, compare_faces
from src.utilities.utils import logger


def choose_random_symbols(n=4):
    """
    It chooses a random symbol from the list of symbols, and then adds it to a list of symbols.

    It does this until the list of symbols is the same length as the number of symbols we want to choose.

    The function returns the list of symbols.

    Let's see what happens when we call the function:

    :param n: the number of symbols to choose, defaults to 4 (optional)
    :return: A list of 4 random symbols
    """
    symbol_options = list(SYMBOL.keys())
    symbol_list = []
    i = 0
    while i < n:
        choice = random.choice(symbol_options)
        if choice not in symbol_list:
            i += 1
            symbol_list.append(choice)
    return symbol_list


class SymbolTask:

    def __init__(self, task_id, uid, width, height):
        logger.info("new symbol task")
        self.width = width
        self.height = height
        self.task_id = task_id
        self.symbol_object = Finger_Symbol()
        self.uid = uid
        self.win_status = False
        self.symbol_to_win = choose_random_symbols()
        self.task_progress = {s: False for s in self.symbol_to_win}
        self.user_confirmed = False
        self.count_frame = 0
        self.relative_count_frame = 0

    def full_operation(self, image):
        """
        It checks if the user has been confirmed, if not, it checks if the user is in the database, if so, it confirms the user and returns the JSON API.
        If the user is not confirmed, it checks the frame count, checks the hand status, checks the single frame, and checks the win status

        :param image: the image that is being processed
        :return: a JSON object that contains the following information:
        - The user's ID
        - The user's name
        - The user's current score
        - The user's current streak
        - The user's current symbol
        - The user's current status
        - The user's current frame count
        - The user's current win status
        - The user's current win streak
        - The
        """
        if not self.user_confirmed:
            self.user_confirmed = compare_faces(self.uid, image)
            if self.user_confirmed:
                return self.create_json_api()
            return None
        self.check_frame_count()
        new_symbol = self.symbol_object.check_hand_status(image)
        if not new_symbol:
            return None
        self.check_single_frame(new_symbol)
        self.check_win_status()
        return self.create_json_api()

    def check_frame_count(self):
        """
        It checks the frame count and cleans the progress if the relative frame count is greater than the maximum frame
        """
        self.count_frame += 1
        self.relative_count_frame += 1
        if self.relative_count_frame > MAX_FRAME:
            logger.info("the client reach the max frames - cleaning progress")
            self.clean_progress()

    def clean_progress(self):
        """
        This function sets the status of all tasks to False
        """
        for key in self.task_progress:
            self.task_progress[key]["status"] = False

    def check_single_frame(self, new_symbol):
        """
        If the task_progress dictionary has a key that matches the new_symbol, then set the value of that key to True

        :param new_symbol: The symbol that was just detected
        """
        for task_point_key in self.task_progress:
            if self.task_progress[task_point_key]:
                continue
            if task_point_key == new_symbol:
                self.task_progress[task_point_key] = True
            break

    def check_win_status(self):
        """
        If all the values in the dictionary are True, then the win_status is set to True
        :return: The win_status is being returned.
        """
        for task_point_key in self.task_progress:
            if not self.task_progress[task_point_key]:
                return False
        self.win_status = True
        logger.info(f"The client complete task -{self.task_id}-!")
        return True

    def create_json_api(self):
        """
        It creates a dictionary with the task_id, status, uid, type, param_values, and user_confirmed
        :return: A dictionary with the task_id, status, uid, type, param_values, and user_confirmed.
        """
        x = {
            "task_id": self.task_id,
            "status": self.win_status,
            "uid": self.uid,
            "type": "symbol",
            "param_values": self.task_progress,
            "user_confirmed": self.user_confirmed

        }
        logger.info(x)
        return x


if __name__ == '__main__':
    logger.info("Debug Symbol Task")
    x = SymbolTask(100, 100, 100, 100)
