import face_recognition
import cv2
from src.services.firebase_manager import FirebaseManager
from src.utilities.utils import logger


def get_face_encoding_by_uid(uid):
    """
    It takes a user id, downloads the image from Firebase Storage, and returns the face encoding of the image

    :param uid: The user id of the person you want to get the face encoding for
    :return: A list of face encodings
    """
    path = FirebaseManager.get_instance().get_file_from_storage(uid)
    face_image = face_recognition.load_image_file(path)
    try:
        face_encoding = face_recognition.face_encodings(face_image)[0]
        logger.info("Found face in the image")
        return [face_encoding]
    except Exception as e:
        logger.error(f"Can't find face in the image due to {e}")
    return []


def compare_faces(uid, frame):
    """
    It takes a frame from the video stream and compares it to the face encoding of the user

    :param uid: The user id of the user you want to compare the face to
    :param frame: The frame of video or image that you want to process
    :return: A boolean value.
    """
    try:
        small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
        rgb_small_frame = small_frame[:, :, ::-1]
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)
        known_face_encodings = get_face_encoding_by_uid(uid)

        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
            if len(matches) == 0:
                return False
            if matches[0]:
                return True
    except Exception as e:
        logger.error(f"Cant compare faces due to {e}")
    return False
