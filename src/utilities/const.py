import json
import numpy as np

with open("conf/data.json") as json_data_file:
    DATA = json.load(json_data_file)

GENERAL_DATA = DATA["General"]
SYMBOL = GENERAL_DATA["Hand_Symbol"]
MAX_FRAME = 999999999999

line_length = lambda x1, y1, x2, y2: np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
