import numpy as np
from math import atan2, degrees

from src.utilities.utils import logger

WRIST_PLACE = 0
MIDDLE_TIP_PLACE = 12
LEFT = "L"
RIGHT = "R"


def create_finger_points_list(finger_points):
    """
    It takes a list of finger points and returns a list of tuples of the x and y coordinates of each finger point

    :param finger_points: a list of points that are the finger points
    :return: A list of tuples containing the x and y coordinates of the finger points.
    """
    finger_points_list = []
    for f_p in finger_points:
        finger_points_list.append((f_p.x, f_p.y))
    return finger_points_list


class Hand_Norm:
    def __init__(self, finger_points):
        self.wrist = (finger_points[WRIST_PLACE].x, finger_points[WRIST_PLACE].y)
        self.middle_tip = (finger_points[MIDDLE_TIP_PLACE].x, finger_points[MIDDLE_TIP_PLACE].y)
        self.finger_points = np.array(create_finger_points_list(finger_points))
        self.normalized_finger_points = []
        self.normalized_points()

    def left_o_right(self):
        """
        If the x-coordinate of the tip of the index finger is greater than the x-coordinate of the tip of the pinky finger, then the hand is a left hand.
        Otherwise, it's a right hand
        :return: The return value is the direction of the hand.
        """
        if self.normalized_finger_points[8][0] - self.normalized_finger_points[16][0] > 0.01:
            return LEFT
        else:
            return RIGHT

    def normalized_points(self):
        """
        The function takes the finger points and the wrist point and rotates them so that the wrist point is at the origin and the finger points are in
        the same relative position to the wrist point
        """
        try:
            deltaY = (self.wrist[1] - self.middle_tip[1])
            deltaX = (self.wrist[0] - self.middle_tip[0])
            r = 1.44 - atan2(deltaY, deltaX)
            self.normalized_finger_points.append(self.wrist)
            R = np.array([[np.cos(r), -np.sin(r)],
                          [np.sin(r), np.cos(r)]])
            for fp in self.finger_points:
                self.normalized_finger_points.append(self.rotate(fp, R))
        except Exception as e:
            logger.error(f"Cant normalized points due to {e}")

    def rotate(self, fp, R):
        """
        > The function takes in a point, and a rotation matrix, and returns the point rotated by the rotation matrix

        :param fp: the point to be rotated
        :param R: rotation matrix
        :return: The rotated point.
        """
        try:
            o = np.atleast_2d(self.wrist)
            p = np.atleast_2d(fp)
            return np.squeeze((R @ (p.T - o.T) + o.T).T)
        except Exception as e:
            logger.error(f"Cant rotate due to {e}")
            return None
