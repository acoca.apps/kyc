import json
import random
import uuid

import cv2

from src.task_obj.symbol_task import SymbolTask
from src.task_obj.trial_task import TrailTask
from src.utilities.utils import logger

task_options = {
    "Trail": TrailTask,
    "Symbol": SymbolTask
}


def choose_random_task():
    try:
        keys = list(task_options.keys())
        chose_key = random.choice(keys)
        logger.info(f"the chosen task is {chose_key}")
        return task_options[chose_key]
    except Exception as e:
        logger.error(f"Cant choose random task due to {e}")
        logger.info("Choosing default task - Symbol")
        return SymbolTask


class WorkingManager:

    def __init__(self):
        self.operation_dict = {}
        self.channel_dict = {}
        self.first_task_send_bool = {}

    def create_new_case(self, user_id, width, height):
        generation_id = str(uuid.uuid4())
        logger.info(f"new case was created - {generation_id}")
        task_init = choose_random_task()
        self.operation_dict[generation_id] = task_init(generation_id, user_id, width, height)
        self.first_task_send_bool[generation_id] = True
        return generation_id

    def add_new_channel(self, generation_id, channel):
        self.channel_dict[generation_id] = channel

    def check_new_frame(self, generation_id, frame_image):
        try:
            image = cv2.flip(frame_image, 1)
            json_response = self.operation_dict[generation_id].full_operation(image)
            if json_response and self.channel_dict.get(generation_id) is not None and json_response['user_confirmed'] and (
                    json_response['param_values'] or self.first_task_send_bool[generation_id]):

                self.first_task_send_bool[generation_id] = False
                try:
                    task_progress = dict(self.operation_dict[generation_id].task_progress)
                    task_progress.update(json_response)
                    logger.info(f"task progress: {task_progress}")
                    self.channel_dict[generation_id].send(json.dumps(task_progress))
                except Exception as e:
                    logger.error(e)
        except Exception as e:
            logger.error(f"Theres been an error at the operation frame, due to : {e}")
