import os
import firebase_admin
from firebase_admin import credentials
from firebase_admin import storage
import cv2

from src.utilities.utils import logger


class FirebaseManager:
    __instance = None

    @staticmethod
    def get_instance():
        """
        If the instance is None, create a new instance, otherwise return the existing instance
        :return: The instance of the class.
        """
        """ Static access method. """
        if FirebaseManager.__instance is None:
            FirebaseManager()
        return FirebaseManager.__instance

    def __init__(self):
        logger.info("initial firebase DB connection")
        cred = credentials.Certificate("conf/firebase-admin-sdk.json")
        firebase = firebase_admin.initialize_app(cred, {'storageBucket': 'tegritykyc-4da91.appspot.com'}, name='storage')
        self.bucket = storage.bucket(app=firebase)
        """ Virtually private constructor. """
        if FirebaseManager.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            FirebaseManager.__instance = self

    def get_file_from_storage(self, uid):
        """
        It downloads the image from the storage bucket, converts it to grayscale, applies a Gaussian blur, and divides the original image by the blurred
        image

        :param uid: The user's unique ID
        :return: The path to the image.
        """
        try:
            file_path = f'userVerificationImages/{uid}.jpeg'
            blob = self.bucket.blob(file_path)
            if not os.path.exists("assets/images"):
                os.makedirs("assets/images")
            if not os.path.exists(f"assets/images/{uid}.jpeg"):
                blob.download_to_filename(f"assets/images/{uid}.jpeg")
                img = cv2.imread(f"assets/images/{uid}.jpeg")
                gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                smooth = cv2.GaussianBlur(gray, (125, 125), 0)
                division = cv2.divide(gray, smooth, scale=255)
                cv2.imwrite(f"assets/images/{uid}.jpeg", division)

            return f"assets/images/{uid}.jpeg"
        except Exception as e:
            logger.error(f"Cant get file from storage due to {e}")
        return ""
