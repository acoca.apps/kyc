from src.finger_obj.finger_movement import Finger_Movement
import cv2
from src.finger_obj.finger_raise import Finger_Raise
from src.utilities.const import SYMBOL, MAX_FRAME


class Finger_Symbol(Finger_Raise):

    def __init__(self):
        super().__init__()
        self._symbol_list = list()

    @property
    def symbol_list(self):
        return self._symbol_list

    def add_symbol(self, symbol):
        """
        It adds a symbol to the end of the list and removes the first symbol in the list if the list is longer than MAX_FRAME.

        :param symbol: The symbol to add to the frame
        """
        if len(self._symbol_list) > MAX_FRAME:
            self._symbol_list.pop(0)
        self._symbol_list.append(symbol)

    def check_hand_status(self, image):
        """
        It checks the hand status, then checks the last phase, then checks the length of the last phase, then adds the symbol to the list of symbols

        :param image: the image to be processed
        :return: The symbol of the last phase.
        """
        last_result = Finger_Movement.check_hand_status(self, image)
        if not last_result:
            return None
        last_phase = super().detect_raise()
        if not last_phase:
            return None
        if len(self._last_phase) != 1:
            return None
        symbol = self.symbol_last_phase(last_phase)
        self.add_symbol(symbol)
        return symbol

    def draw_hands_to_image(self, image):
        """
        It draws the hand to the image, then it detects the last phase of the hand, then it draws the symbol of the last phase to the image

        :param image: the image to draw the hands on
        :return: The image with the symbol drawn on it.
        """
        image = Finger_Movement.draw_hands_to_image(self, image)
        last_phase = super().detect_raise()
        symbol = self.symbol_last_phase(last_phase)
        if symbol:
            for p in self.normalized.normalized_finger_points:
                image = cv2.putText(image, ".", (int(p[0] * image.shape[1]), int(p[1] * image.shape[0])),
                                    cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
            image = cv2.putText(image, str(symbol), (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
        return image

    def symbol_last_phase(self, last_phase):
        """
        It takes the last phase of the game and returns the symbol that was played

        :param last_phase: the last phase of the symbol
        :return: The symbol that is being returned is the symbol that is being returned is the symbol that is being returned is the symbol that is being
        returned is the symbol that is being returned is the symbol that is being returned is the symbol that is being returned is the symbol that is
        being returned is the symbol that is being returned is the symbol that is being returned is the symbol that is being returned is the symbol that
        """
        lp = self._last_phase[0]
        for symbol in SYMBOL.keys():
            flag = True
            for i in range(5):
                if SYMBOL[symbol][i] != lp[i]:
                    flag = False
                    break
            if flag:
                return symbol
        return None
