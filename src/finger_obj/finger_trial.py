import cv2
from src.finger_obj.finger_movement import Finger_Movement
from src.utilities.const import GENERAL_DATA, MAX_FRAME
from src.utilities.utils import logger


class Finger_Trial(Finger_Movement):
    def __init__(self, hand=1, finger=1):
        super().__init__()
        self._fingers_points = list()
        self._hand_number = hand
        self._hand_name = GENERAL_DATA["Hands"][hand]
        self._finger_number = finger
        self._finger_name = GENERAL_DATA["Fingers"][finger]
        self._tip_of_the_finger = GENERAL_DATA["Fingers_Top"][finger]

    @property
    def fingers_points(self):
        return self._fingers_points

    def check_hand_status(self, image):
        """
        > If the hand is detected, add a point to the hand's trajectory

        :param image: the image to be processed
        :return: The image shape is being returned.
        """
        last_result = super().check_hand_status(image)
        if not last_result:
            return None
        if not last_result.multi_handedness:
            return None
        if len(last_result.multi_handedness) > 1:
            logger.error("to much hands")
            return None
        return self.add_point(image.shape[1], image.shape[0])

    def add_point(self, width, height):
        """
        It takes the coordinates of the tip of the finger and adds it to a list of points

        :param width: the width of the image
        :param height: The height of the image
        :return: The x and y coordinates of the fingertip.
        """
        landmark = self._last_result.multi_hand_landmarks[0].landmark
        fingertip = landmark[self._tip_of_the_finger]
        if len(self._fingers_points) > 25:
            self._fingers_points.pop(0)
        self._fingers_points.append((int(fingertip.x * width), int(fingertip.y * height)))
        return int(fingertip.x * width), int(fingertip.y * height)

    def draw_hands_to_image(self, image):
        """
        It draws a line between each finger point and the next finger point

        :param image: The image to draw the hands on
        :return: The image with the lines drawn on it.
        """
        image = super().draw_hands_to_image(image)
        a = self._fingers_points
        for i in range(0, len(a) - 1):
            image = cv2.line(image, a[i], a[i + 1], (255, 255, 255), 7)
        return image

    def draw_hands_and_points(self, image):
        """
        It draws the hands and points to the image

        :param image: The image to draw on
        :return: The image with the lines drawn on it.
        """
        image = super().draw_hands_to_image(image)
        if self._last_result:
            self.add_point(image.shape[1], image.shape[0])
        a = self._fingers_points
        for i in range(0, len(a) - 1):
            image = cv2.line(image, a[i], a[i + 1], (255, 255, 255), 7)
        return image
