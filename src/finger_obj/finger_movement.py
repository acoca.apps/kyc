import cv2
import mediapipe as mp


class Finger_Movement:
    def __init__(self):
        self._mp_drawing = mp.solutions.drawing_utils
        self._mp_hands = mp.solutions.hands
        self._last_result = None

    def check_hand_status(self, image):
        """
        It takes an image, converts it to RGB, and then uses the `Hands` class to process the image and return the results

        :param image: The image to process
        :return: The results of the hand detection.
        """
        with self._mp_hands.Hands(min_detection_confidence=0.65, min_tracking_confidence=0.65) as hands:
            image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)
            results = hands.process(image)
            if results.multi_hand_landmarks:
                self._last_result = results
        return results

    def draw_hands_to_image(self, image):
        """
        It takes an image, runs it through the hand-detection model, and draws the detected hands on the image

        :param image: The image to draw the hands on
        :return: The image with the hand landmarks drawn on it.
        """
        with self._mp_hands.Hands(min_detection_confidence=0.65, min_tracking_confidence=0.65) as hands:
            image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)
            results = hands.process(image)
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            if results.multi_hand_landmarks:
                self._last_result = results
                for hand_landmarks in results.multi_hand_landmarks:
                    self._mp_drawing.draw_landmarks(image, hand_landmarks, self._mp_hands.HAND_CONNECTIONS)
            else:
                self._last_result = None
            return image
