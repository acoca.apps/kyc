from src.finger_obj.finger_movement import Finger_Movement
import numpy as np
import cv2
from src.utilities.Hand_Norm import Hand_Norm


class Finger_Raise(Finger_Movement):

    def __init__(self):
        super().__init__()
        self._last_phase = None

    def draw_hands_to_image(self, image):
        """
        It draws the hands to the image, detects the raise, counts the last phase raising, and draws the normalized finger points to the image

        :param image: The image to draw on
        :return: The image with the hands drawn on it.
        """
        image = super().draw_hands_to_image(image)
        self.detect_raise()
        if self._last_phase:
            sum_all = self.counting_last_phase_raising()
            image = cv2.putText(image, str(sum_all), (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
            for p in self.normalized.normalized_finger_points:
                image = cv2.putText(image, ".", (int(p[0] * image.shape[1]), int(p[1] * image.shape[0])),
                                    cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
        return image

    def detect_raise(self):
        """
        This function takes in the normalized hand landmarks and returns a list of 5 numbers, each number representing the raise of a finger
        :return: The return value is a list of lists. Each list contains the location of the fingers of a hand.
        """
        multi_hands_finger_location = list()
        if not self._last_result:
            self._last_phase = multi_hands_finger_location
            return None
        lr = self._last_result
        for hand in lr.multi_hand_landmarks:
            finger_locations = [0, 0, 0, 0, 0]
            i = 1
            self.normalized = Hand_Norm(hand.landmark)
            for j in range(0, 5):
                finger_points = self.normalized.normalized_finger_points[i:i + 4]
                if j == 0:
                    x = self.detect_raise_specific_finger(finger_points, self.normalized.left_o_right())
                else:
                    x = self.detect_raise_specific_finger(finger_points, None)
                finger_locations[j] = x
                i += 4
            multi_hands_finger_location.append(finger_locations)
        self._last_phase = multi_hands_finger_location
        return self._last_phase

    def counting_last_phase_raising(self):
        """
        This function counts the number of 1's in the last phase of the game.
        :return: The number of 1's in the last phase.
        """
        count = 0
        for hand in self._last_phase:
            for x in hand:
                if x == 1:
                    count += 1
        return count

    def detect_raise_specific_finger(self, finger_points, direction):
        """
        It takes in the finger points and the direction of the hand and returns 1 if the finger is raised and -1 if it is not

        :param finger_points: The points of the finger that we want to detect the raise for
        :param direction: This is the direction of the hand. It can be either "R" or "L"
        :return: 1 if the finger is raised and -1 if the finger is not raised.
        """
        finger_points = np.array(finger_points)
        for i in range(len(finger_points) - 1):
            distance = np.linalg.norm(finger_points[i] - finger_points[i + 1])
            if distance < 0.001:
                return 0
        if direction:  # Thumb
            if direction == "R":
                if finger_points[1][0] - finger_points[3][0] > 0.001:
                    return 1
                return -1
            else:  # Left Hand
                if finger_points[1][0] - finger_points[3][0] > 0.001:
                    return -1
                return 1
        else:  # Rest of the hand
            if finger_points[1][1] - finger_points[3][1] > 0.01:
                return 1
            return -1
