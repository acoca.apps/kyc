import random
import time

import cv2

from src.finger_obj.finger_movement import Finger_Movement
from src.finger_obj.finger_raise import Finger_Raise
from src.finger_obj.finger_symbol import Finger_Symbol
from src.finger_obj.finger_trial import Finger_Trial
from src.utilities.face_recognition_utils import get_face_encoding_by_uid, compare_faces
from src.utilities.utils import logger

type_dict = {
    "Trial": Finger_Trial,
    "Symbol": Finger_Symbol}


class WorkingManager:

    def __init__(self):
        task_type = choose_random_finger_option()
        self.task = task_type.create_random_task()


def choose_random_finger_option():
    """
    > Choose a random key from the dictionary, and then return the value of that key
    :return: A function object
    """
    keys = list(type_dict.keys())
    chose_key = random.choice(keys)
    logger.info(f"the chosen key is {chose_key}")
    return type_dict[chose_key]()


if __name__ == '__main__':
    logger.info("Server started - for local use")
    Detector = choose_random_finger_option()
    cap = cv2.VideoCapture(0)
    number = 0
    specific_num = -1
    None_count = 0
    Same_count = 0
    user_confirmed = False
    while cap.isOpened():
        success, image = cap.read()

        if not success:
            logger.info("Ignoring empty camera frame.")
            continue
        if not user_confirmed:
            user_confirmed = compare_faces("omer", image)
            if user_confirmed:
                logger.info("User Confirmed!")
            else:
                logger.info("Missing User")
            continue
        image = Detector.draw_hands_and_points(image)
        cv2.imshow('MediaPipe Hands', image)
        if cv2.waitKey(5) & 0xFF == 27:
            break
    cap.release()
