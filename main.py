import argparse
import asyncio
import json
import logging
import os
import ssl
import uuid

from aiohttp import web
import aiohttp_cors
from aiortc import MediaStreamTrack, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaBlackhole, MediaPlayer, MediaRelay
from src.services.working_manager import WorkingManager
from src.utilities.utils import logger

ROOT = os.path.dirname(__file__)

pcs = set()
relay = MediaRelay()


class VideoTransformTrack(MediaStreamTrack):
    """
    A video stream track that transforms frames from an another track.
    """

    kind = "video"

    def __init__(self, track, generation_id):
        super().__init__()  # don't forget this!
        self.track = track
        self.generation_id = generation_id

    async def recv(self):
        """
        > When a new frame is received, it is sent to the working manager to be processed
        :return: The frame is being returned.
        """
        frame = await self.track.recv()
        img = frame.to_ndarray(format="bgr24")

        working_manager.check_new_frame(self.generation_id, frame_image=img)

        return frame


async def index(request):
    """
    It opens the file `index.html` in the root directory of the project, reads its contents, and returns a response with the content type `text/html` and
    the contents of the file

    :param request: a Request instance
    :return: The index.html file is being returned.
    """
    content = open(os.path.join(ROOT, "src/webclient/index.html"), "r").read()
    return web.Response(content_type="text/html", text=content)


async def javascript(request):
    """
    It reads the contents of the file `client.js` and returns it as a response

    :param request: This is the request object that contains the information about the request
    :return: The content of the client.js file is being returned.
    """
    content = open(os.path.join(ROOT, "src/webclient/client.js"), "r").read()
    return web.Response(content_type="application/javascript", text=content)


async def offer(request):
    """
    It creates a new RTCPeerConnection, sets up a few event handlers, and then sends an answer back to the client

    :param request: The request object
    :return: The answer is being returned.
    """
    params = await request.json()
    offer = RTCSessionDescription(sdp=params["sdp"], type=params["type"])
    user_id = params['user_id']
    width = int(params['width'])
    height = int(params['height'])
    generation_id = working_manager.create_new_case(user_id=user_id, width=width, height=height)

    pc = RTCPeerConnection()
    pc_id = "PeerConnection(%s)" % uuid.uuid4()
    pcs.add(pc)

    def log_info(msg, *args):
        logger.info(pc_id + " " + msg, *args)

    log_info("Created for %s", request.remote)

    # prepare local media
    player = MediaPlayer(os.path.join(ROOT, "src/webclient/demo-instruct.wav"))
    recorder = MediaBlackhole()

    @pc.on("datachannel")
    def on_datachannel(channel):
        @channel.on("message")
        def on_message(message):
            json_msg = json.loads(message)
            if json_msg['type'] == "initChannel":
                working_manager.add_new_channel(generation_id=json_msg['generationID'], channel=channel)
                logger.info("init channel called")

    @pc.on("connectionstatechange")
    async def on_connectionstatechange():
        log_info("Connection state is %s", pc.connectionState)
        if pc.connectionState == "failed":
            await pc.close()
            pcs.discard(pc)

    @pc.on("track")
    def on_track(track):
        log_info("Track %s received", track.kind)

        if track.kind == "audio":
            pc.addTrack(player.audio)
            recorder.addTrack(track)
        elif track.kind == "video":
            pc.addTrack(
                VideoTransformTrack(
                    relay.subscribe(track),
                    generation_id=generation_id,
                )
            )

        @track.on("ended")
        async def on_ended():
            log_info("Track %s ended", track.kind)
            await recorder.stop()

    # handle offer
    await pc.setRemoteDescription(offer)
    await recorder.start()

    # send answer
    answer = await pc.createAnswer()
    await pc.setLocalDescription(answer)

    return web.Response(
        content_type="application/json",
        text=json.dumps(
            {"sdp": pc.localDescription.sdp, "type": pc.localDescription.type, "generation_id": generation_id}
        ),
    )


async def on_shutdown(app):
    # close peer connections
    coros = [pc.close() for pc in pcs]
    await asyncio.gather(*coros)
    pcs.clear()


app = web.Application()
cors = aiohttp_cors.setup(app)
app.on_shutdown.append(on_shutdown)
app.router.add_get("/", index)
app.router.add_get("/client.js", javascript)
app.router.add_post("/offer", offer)

for route in list(app.router.routes()):
    cors.add(route, {
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
            allow_methods="*"
        )
    })

if __name__ == "__main__":
    logger.info("initial WebRTC server, default port 55555")
    working_manager = WorkingManager()

    parser = argparse.ArgumentParser(
        description="WebRTC audio / video / data-channels demo"
    )
    parser.add_argument("--cert-file", help="SSL certificate file (for HTTPS)")
    parser.add_argument("--key-file", help="SSL key file (for HTTPS)")
    parser.add_argument(
        "--host", default="0.0.0.0", help="Host for HTTP server (default: 0.0.0.0)"
    )
    parser.add_argument(
        "--port", type=int, default=55555, help="Port for HTTP server (default: 55555)"
    )
    parser.add_argument("--record-to", help="Write received media to a file."),
    parser.add_argument("--verbose", "-v", action="count")
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if args.cert_file:
        ssl_context = ssl.SSLContext()
        ssl_context.load_cert_chain(args.cert_file, args.key_file)
    else:
        ssl_context = None

    web.run_app(
        app, access_log=None, host=args.host, port=args.port, ssl_context=ssl_context
    )
