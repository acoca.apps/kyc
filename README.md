# KYC - Know Your Costumer

This is our Tutorial for the KYC server. Giving people an easy way to use kyc's API.

## Outline

 - [Installation](#installation)


## Libraries

This project is using several libraries and frameworks:

 - [opencv_python]() - Image operations
 - [face_recognition]() - Face recognition
 - [mediapipe]() - Motion Detection
 - [firebase_admin]() - Database

## Installation

### Dependencies

Make sure you have Cmake, Git and Docker installed. Type into the terminal:

```bash
$ Cmake -v
```


```bash
$ git --version
```

```bash
$ docker --version
```

### GitHub

The first step is to register a [github account](https://github.com/) (if you don't have one yet) which allows you
to store your code for free on the github servers.

You probably need to [upload your ssh key](https://help.github.com/articles/generating-ssh-keys) to Github
in order to get or push repositories:

### Fork and Clone

clone your version of this repository locally:

```bash
$ git clone git@gitlab.com:acoca.apps/kyc.git
```

### Setup

Run the task to create the docker-image & docker-container:

```bash
$ docker-compose up --build -d
```

### Running

The docker-compose create the full run flow. to make sure that we are running without errors:

```bash
$ docker logs kyc_server
```

See that there aren't any errors at the log file.

### Wrapping Up ###

At this point you have a deployed API application.
The server default port is *55555*.

If you make changes to your app, simply run:

```bash
$ git add .
$ git commit -am "describe my changes here"
$ git push
```

and the updated code will be pushed.