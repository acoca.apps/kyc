FROM python:3.7

COPY . /app
WORKDIR /app/

RUN apt-get update && apt-get -y install python3-pip \
&& apt-get install -y cmake \
&& apt-get install -y ffmpeg libsm6 libxext6 libgl1 \
&& pip install -r requirements.txt

ENV MODE=production
CMD ["python", "main.py"]